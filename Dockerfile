FROM python:2.7-alpine
COPY . /app
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["main.py"]

#docker build -t acm.dp.gcp.job.trigger:latest .

########## Tag & Push to GCR
## docker tag acm.dp.gcp.job.trigger us.gcr.io/dataplatform-1363/acm.dp.gcp.job.trigger:latest
## gcloud docker -- push us.gcr.io/dataplatform-1363/acm.dp.gcp.job.trigger:latest
##
## Need to update entrypoint.sh to corresponding with number of required arguments


## docker build -t cloud.sdk.acm.inter -f Dockerfile_acm_inter_authen .
## docker run -ti --name gcloud.config.acm.inter cloud.sdk.acm.inter gcloud auth activate-service-account acm-inter-bq-loader@acm-inter.iam.gserviceaccount.com --key-file acm-inter-bq-loader.json

#########Pull images
##sudo docker pull us.gcr.io/dataplatform-1363/acm.dp.gcp.job.trigger:latest

#########Run docker container
##sudo docker run --restart=always --name flask-jenkins-trigger -d -p 5000:5000 us.gcr.io/dataplatform-1363/acm.dp.gcp.job.trigger
