const rp = require('sync-request');
exports.processFile = (event, callback) => {
	const file = event.data
	const context = event.context
    console.log(context)
  console.log('Processing file: ' + file.name);

   if (file.resourceState === 'not_exists') {
     console.log('not exists')
   // This is a file deletion event, so skip it
   callback();
   return;
 }

 let folderStr = "/";
 if(file.name.indexOf(folderStr) === -1) {
   console.log('folder')
   callback();
   return;
 }

 if(file.name.indexOf("schema") !== -1) {
   console.log('schema file')
   callback();
   return;
 }

 if(file.name.indexOf("json") !== -1) {
   console.log('json file')
   callback();
   return;
 }

 if(file.name.indexOf("sha256") !== -1) {
   var request_path = `http://35.197.136.25:5000/jenkins_build/acm-staging/18c700fdcf4345e6968717c8f4db4c61?filename=${event.data.name}`
   var res = rp('GET',request_path)
   console.log(`Request to path: `+request_path)
   console.log(res.getBody());
   callback();
   return;
 }

  console.log(`file not match exist type.`)
  callback();
}


//gcloud beta functions deploy processFile --trigger-resource test-cloud-trigger-jenkins --trigger-event google.storage.object.finalize
