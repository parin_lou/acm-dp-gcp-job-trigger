# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START app]
import logging

import os
import re
import requests
from flask import Flask, request
import yaml
import table_info

app = Flask(__name__)


def get_auth_key():
    return '18c700fdcf4345e6968717c8f4db4c61'


@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return "web service for trigger jenkins job"

@app.route('/jenkins_build/<env>/<auth_key>')
def jenkins_build(env, auth_key):

    if auth_key != get_auth_key():
        return 'Not authorized'
    else:
        gcs_file_name = request.args.get('filename')
        table_information = table_info.TableInfo(env, gcs_file_name.split('/')[1])
        jenkins_job_name = get_job_name_new(gcs_file_name, table_information.get_table_type())
        crumb_token = get_jenkins_crumb(table_information.get_jenkins_env('id'),table_information.get_jenkins_env('token'),table_information.get_jenkins_env('path'))
        header = {
            'Jenkins-Crumb': crumb_token
            }
        if (jenkins_job_name):
            if jenkins_job_name:
                url = 'http://' + table_information.get_jenkins_env('id') + ':' + table_information.get_jenkins_env(
                    'token') + '@' + table_information.get_jenkins_env(
                    'path') + '/job/' + table_information.get_table_dir() + '/job/' + jenkins_job_name + '/buildWithParameters'

                logging.info(url)
                response = requests.post(url,headers=header)
                if response.status_code != 201:
                    return '[FAILED] file ' + gcs_file_name + ' with job name ' + jenkins_job_name + ' submited to jenkins with return code : ' + str(
                        response.status_code)+str(response._content)

                return '[SUCCESS] file ' + gcs_file_name + ' with job name ' + jenkins_job_name + ' submited to jenkins with return code : ' + str(
                    response.status_code)


        else:
            return '[FAILED] this table is not in jenkins.'

def get_jenkins_crumb(jenkins_id,jenkins_token,jenkins_path):
    url = 'http://'+jenkins_id+':'+jenkins_token+'@'+jenkins_path+'//crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)'
    response = requests.get(url)

    crumb_token = response._content.split(':')[1]
    if(response.status_code == 200):
        logging.info('[SUCCESS] status code ' + str(response.status_code))
        return crumb_token
    else:
        logging.error('[FAILED] with status code ' + str(response.status_code))
        return None



# format is [database]_[table]_[daily/hourly]
def get_job_name(fullpath):
    job_name_format = 'load-bq-{}{}'
    split_filename = fullpath.split('/')
    database_name = split_filename[0]
    table_name = split_filename[1]
    basename = os.path.splitext(fullpath)[0]
    split_basename = basename.split('_')
    table_type = check_table_type(table_name)

    if (table_type == 'both'):
        if (re.match('^\d{2}\d{2}\d{2}$', split_basename[-1])):
            job_name = job_name_format.format(database_name + '_' + table_name, '_hourly')
            return job_name
        else:
            job_name = job_name_format.format(database_name + '_' + table_name, '_daily')
            return job_name
    elif (table_type == 'daily'):
        job_name = job_name_format.format(database_name + '_' + table_name, '')
        return job_name
    else:
        logging.info ('No job with table name : ' + table_name + ' in jenkins')
        return ''


def get_job_name_new(fullpath, job_type):
    job_name_format = 'load-bq-{}{}'
    split_filename = fullpath.split('/')
    database_name = split_filename[0]
    table_name = split_filename[1]
    basename = os.path.splitext(fullpath)[0]
    split_basename = basename.split('_')

    if (job_type == 'table-multi'):
        if (re.match('^\d{2}\d{2}\d{2}$', split_basename[-1])):
            job_name = job_name_format.format(database_name + '_' + table_name, '_hourly')
            return job_name
        else:
            job_name = job_name_format.format(database_name + '_' + table_name, '_daily')
            return job_name
    elif (job_type == 'table-daily'):
        job_name = job_name_format.format(database_name + '_' + table_name, '')
        return job_name
    else:
        logging.info ('No job with table name : ' + table_name + ' in jenkins')
        return ''


def check_table_type(table_name):
    table_daily_list = []
    table_both_list = []
    with open("profile_config.yaml") as stream:
        try:
            table_dict = yaml.load(stream)
            logging.info (table_dict)
            table_both_list = table_dict['table-multi']
            table_daily_list = table_dict['table-daily']
        except yaml.YAMLError as exc:
            logging.info(exc)

    logging.info('Table with both type daily and hourly is : ' + str(table_both_list))
    logging.info ('Table with daily type is : ' + str(table_daily_list))
    if (table_name in table_both_list):
        return 'both'
    elif (table_name in table_daily_list):
        return 'daily'
    else:
        return ''


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='0.0.0.0', debug=True)
# [END app]
