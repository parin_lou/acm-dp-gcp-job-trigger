import yaml


class TableInfo:
    dir_name = ''
    table_type = ''
    profile_config_path_format = 'profile/{}/profile_config.yaml'
    env_config_path_format = 'profile/{}/env_config.yaml'
    def __init__(self,env,table_name):
        with open(self.profile_config_path_format.format(env)) as stream:
            try:
                table_dict = yaml.load(stream)
                print (table_dict)
                self.table_dict = table_dict
            except yaml.YAMLError as exc:
                print(exc)

        with open(self.env_config_path_format.format(env)) as stream:
            try:
                jenkins_env = yaml.load(stream)
                print (jenkins_env)
                self.jenkins_env = jenkins_env
            except yaml.YAMLError as exc:
                print(exc)

        self.create_table_info(table_name)

    def create_table_info(self,table_name):
        for table_type in self.table_dict:
            for dir_name in self.table_dict[table_type]:
                for table in self.table_dict[table_type][dir_name]:
                    if(table == table_name):
                        self.dir_name = dir_name
                        self.table_type = table_type

    def get_table_dir(self):
        return self.dir_name

    def get_table_type(self):
        return self.table_type

    def get_jenkins_env(self,key):
        return self.jenkins_env[key]








