import unittest

from mock import patch,MagicMock,mock_open

# from main import check_table_type
import main

class TestMain(unittest.TestCase):
    # def test_get_jobname(self):
    #     test_value = getJobname('test/tmn_profile/test_test3_20170301.sha256')


    #
    # def test_check_table_type(self):
    #     test = check_table_type('tmn_profile')
    #     print test


    def test_check_table_type_table_not_exist(self):

        data = 'table-multi:\
                    - tmn_profile\
                    - test_trigger\
                table-daily:\
                  -\
                '

        with patch("__builtin__.open", mock_open(read_data=data)) as mock_file:
            assert open("config.yaml").read() == data
            mock_file.assert_called_with("config.yaml")
            main.check_table_type('test')

            #testMain = check_table_type('tmn_profile')

    @patch('main.check_table_type')
    def test_get_job_name_not_exist(self,_patch_table_type):
        _patch_table_type.return_value = ''
        result = main.get_job_name('test/test/test_not_exist')
        expect = ''

        self.assertEqual(result,expect)

    @patch('re.match')
    @patch('main.check_table_type')
    def test_get_job_name_both_type_hourly(self,_patch_table_type,_patch_re):
        _patch_table_type.return_value = 'both'
        _patch_re.return_value = True
        result = main.get_job_name('database_name/table_name/file')
        expect = 'load-bq-database_name_table_name_hourly'

        self.assertEqual(result,expect)

    @patch('re.match')
    @patch('main.check_table_type')
    def test_get_job_name_both_type_daily(self, _patch_table_type, _patch_re):
        _patch_table_type.return_value = 'both'
        _patch_re.return_value = False
        result = main.get_job_name('database_name/table_name/file')
        expect = 'load-bq-database_name_table_name_daily'

        self.assertEqual(result, expect)

    @patch('main.check_table_type')
    def test_get_job_name_daily_type(self, _patch_table_type):
        _patch_table_type.return_value = 'daily'
        result = main.get_job_name('database_name/table_name/file')
        expect = 'load-bq-database_name_table_name'

        self.assertEqual(result, expect)
