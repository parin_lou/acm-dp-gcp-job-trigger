import os
import unittest
from mock import MagicMock,patch
from main import app
import main

class MainTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def test_home_return_status_code(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code,200)

    def test_home_return_data(self):
        result = self.app.get('/')
        self.assertEqual(result.data,'web service for trigger jenkins job')

    def test_request_with_wrong_auth_code(self):
        auth_key = 'wrong_auth_key'
        result = self.app.get('/beamjob/temp/'+auth_key)
        self.assertEqual(result.status_code,200)
        self.assertEqual(result.data,'Not authorized')

    @patch('main.get_auth_key')
    @patch('main.get_job_name')
    def test_request_with_daily_job_exist(self,_patch_job_name,_patch_auth_key):
        _patch_auth_key.return_value = 'test_auth_key'
        _patch_job_name.return_value = 'load_bq-test-daily'

        result = self.app.get('/beamjob/temp/test_auth_key')
        print result.data
        self.assertEqual(result.status_code,200)


        #main.get_auth_key() = MagicMock(name='get_auth_key')

    @patch('main.get_auth_key')
    @patch('main.get_job_name')
    def test_request_with_daily_job_not_exist(self, _patch_job_name, _patch_auth_key):
        _patch_auth_key.return_value = 'test_auth_key'
        _patch_job_name.return_value = ''

        expect_data = '[FAILED] this table is not in jenkins.'

        expect_status_code = 200

        result = self.app.get('/beamjob/temp/test_auth_key')
        print result.data
        self.assertEqual(result.status_code, expect_status_code)
        self.assertEqual(result.data,expect_data)


    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
